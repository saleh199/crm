<?php

Breadcrumbs::register('general', function ($breadcrumbs, $pageBreadcrumbs) {
    /* @var DaveJamesMiller\Breadcrumbs\Generator $breadcrumbs*/
    $breadcrumbs->push('الصفحة الرئيسية', route('home'));

    if (is_array($pageBreadcrumbs) && count($pageBreadcrumbs) > 0) {
        foreach ($pageBreadcrumbs as $item) {
            $breadcrumbs->push($item['title'], $item['url']);
        }
    }
});
