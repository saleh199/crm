<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'DashboardController@index')->name('home')->middleware('auth');


route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
route::post('login', 'Auth\LoginController@login');
// Password Reset Routes...
route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
route::post('password/reset', 'Auth\ResetPasswordController@reset');

route::get('logout', 'Auth\LoginController@logout')->name('logout');


Route::group(['prefix'=> 'users' , 'middleware' =>'auth'] , function () {
    route::get('/', 'UsersController@index')->name('users_list');
    route::get('/create', 'UsersController@create')->name('users_create');
    route::get('/{id}/view', 'UsersController@view')->name('users_view');
    route::post('/{id}/department', 'UsersController@departments');
    route::get('/{id}/department/{department_id}', 'UsersController@departments');
    route::post('/', 'UsersController@store');
    route::get('/{id}/edit', 'UsersController@edit')->name('users_edit');
    route::put('/{id}', 'UsersController@update');
    route::delete('/{id}', 'UsersController@destroy');

});

Route::group(['prefix'=> 'departments' , 'middleware' =>'auth'] , function () {
    route::get('/', 'DepartmentsController@index')->name('departments_list');
    route::get('/create', 'DepartmentsController@create')->name('departments_create');
    route::post('/', 'DepartmentsController@store');
    route::get('/{id}/edit', 'DepartmentsController@edit')->name('departments_edit');
    route::put('/{id}', 'DepartmentsController@update');
    route::delete('/{id}', 'DepartmentsController@destroy');
});

Route::group(['prefix'=> 'clients' , 'middleware' =>'auth'] , function () {
    route::get('/', 'ClientsController@index')->name('clients_list');
    route::get('/create', 'ClientsController@create')->name('clients_create');
    route::post('/', 'ClientsController@store');
    route::get('/{id}/edit', 'ClientsController@edit')->name('clients_edit');
    route::put('/{id}', 'ClientsController@update');
    route::delete('/{id}', 'ClientsController@destroy');
});

Route::group(['prefix'=> 'tasks' , 'middleware' =>'auth'] , function () {
    route::get('/', 'TasksController@index')->name('tasks_list');
    route::get('/create', 'TasksController@create')->name('tasks_create');
    route::get('/{id}', 'TasksController@view');
    route::get('/{id}/view', 'TasksController@view')->name('tasks_view');
    route::post('/{id}/note', 'TasksController@noteStore');
    route::post('/', 'TasksController@store');
    route::get('/{id}/edit', 'TasksController@edit')->name('tasks_edit');
    route::put('/{id}', 'TasksController@update');
    route::delete('/{id}', 'TasksController@destroy');

    Route::group(['prefix' => '{task_id}/notes'], function (){
        route::get('/', 'TaskNotesController@index')->name('tasks_notes_list');
        route::get('/create', 'TaskNotesController@create')->name('tasks_notes_create');
        route::post('/', 'TaskNotesController@store');
        route::get('/{id}/edit', 'TaskNotesController@edit')->name('tasks_notes_edit');
        route::put('/{id}', 'TaskNotesController@update');
        route::delete('/{id}', 'TaskNotesController@destroy');
    });

});


Route::group(['prefix'=> 'orders' , 'middleware' =>'auth'] , function () {
    route::get('/', 'OrdersController@index')->name('orders_list');
    route::get('/create', 'OrdersController@create')->name('orders_create');
    route::get('/{id}', 'OrdersController@view')->name('orders_view');
    route::get('/{id}/view', 'OrdersController@view');
    route::post('/{id}/note', 'OrdersController@noteStore')->name('orders_notes_list');
    route::post('/', 'OrdersController@store');
    route::get('/{id}/edit', 'OrdersController@edit')->name('orders_edit');
    route::put('/{id}', 'OrdersController@update');
    route::delete('/{id}', 'OrdersController@destroy');

    route::get('/{order_id}/invoice/create' , 'OrdersController@invoiceCreate')->name('invoices_create');
    route::post('/{order_id}/invoice/create' , 'OrdersController@invoiceStore');
    route::get('/{order_id}/invoice/{id}' , 'OrdersController@invoiceEdit')->name('invoices_edit');
    route::put('/{order_id}/invoice/{id}' , 'OrdersController@invoiceUpdate');

    Route::group(['prefix' => '{order_id}/notes'], function (){
        route::get('/', 'OrderNotesController@index')->name('orders_notes_list');
        route::get('/create', 'OrderNotesController@create')->name('orders_notes_create');
        route::post('/', 'OrderNotesController@store');
        route::get('/{id}/edit', 'OrderNotesController@edit')->name('orders_notes_edit');
        route::put('/{id}', 'OrderNotesController@update');
        route::delete('/{id}', 'OrderNotesController@destroy');
    });

});


Route::group(['prefix'=> 'invoices' , 'middleware' =>'auth'] , function () {
    route::get('/', 'InvoicesController@index')->name('invoices_list');
    route::delete('/{id}', 'InvoicesController@destroy');
});
