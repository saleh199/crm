@extends('layouts.master')

@section('title' , $pageInfo['page_name'].' - '. $pageInfo['title'])


@section('content')

<div class="row">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($model, [ 'url' => $pageInfo['form_url'] , 'method' => 'post',  'class' => 'col-lg-12', 'files' => true]) !!}
        {{ method_field($pageInfo['form_method']) }}
        {!! Form::token() !!}
        <div class="form-group">
            {!! Form::label('الإسم') !!}
            {!! Form::text('name', null, ['placeholder' =>'الإسم', 'class' => 'form-control', 'id' => 'name']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('البريد الإلكتروني') !!}
            {!! Form::email('email', null, ['placeholder' => 'example@gmail.com', 'class' => 'form-control', 'id' => 'email']) !!}
        </div>


        <div class="form-group">
            {!! Form::label("كلمة المرور") !!}
            {!! Form::password('password',  [ 'class' => 'form-control', 'id' => 'password']) !!}
        </div>

        <div class="form-group">
            {!! Form::label("تأكيد كلمة المرور") !!}
            {!! Form::password('password_confirmation', [ 'class' => 'form-control', 'id' => 'password_confirmation']) !!}
        </div>


        <div class="form-group">
            {!! Form::submit( "حفظ", ['class' => 'btn btn-primary']) !!}
            <a href="{{ url('/users') }}" class="btn btn-warning">الغاء الأمر</a>
        </div>


    {!! Form::close() !!}

</div>

@endsection
