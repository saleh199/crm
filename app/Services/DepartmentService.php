<?php
/**
 * Created by PhpStorm.
 * User: amro
 * Date: 12/3/16
 * Time: 12:03 PM
 */
namespace App\Services;

use App\Models\Department;
use App\Models\User;
use App\Models\UserDepartment;

/**
 * Class DepartmentService
 * @package App\Services
 */
class DepartmentService{


    function __construct()
    {
    }

    /**
     * @param $criteria
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    function getList($criteria = []){
        $res = $this->resolveCriteria($criteria)->get();
        return $res;
    }

    function getOne($id){
        $res = Department::findOrFail($id);
        return $res;
    }

    protected function resolveCriteria($data = [])
    {
        $query = Department::Query();

        if (array_key_exists('columns', $data)) {
            $query = $query->select($data['columns']);
        }

        if (array_key_exists('department_id', $data)) {
            $query = $query->where('id', $data['department_id']);
        }

        if (array_key_exists('name', $data)) {
            $query = $query->where('name', 'LIKE', "'%" . $data['name'] . "%'");
        }

        if (array_key_exists('description', $data)) {
            $query = $query->where('description', 'LIKE'  , "%". $data['description'] ."%");
        }

        if (array_key_exists('created_at', $data)) {
            $query = $query->where('created_at', "LIKE", "%".$data['created_at']."%");
        }

        if (array_key_exists('limit', $data) && array_key_exists('offset', $data)) {
            $query = $query->take($data['limit']);
            $query = $query->skip($data['offset']);
        }

        return $query;
    }

    function create( $dataIn = [], Department &$Department){
        $this->mapDataModel($dataIn , $Department);

        $Department->save();
    }

    public function mapDataModel($data, Department &$model)
    {
        $attribute = [
            'name',
            'description',
            'is_admin'
        ];

        foreach ($attribute as $val) {
            if (array_key_exists($val, $data)) {
                $model->$val = $data[$val];
            }
        }
    }

    function update($dataIn = [] , Department &$Department ){

        $this->mapDataModel($dataIn , $Department);
        $Department->save();
    }

    function delete($id){
        $res = $this->getOne($id);

        $res->delete();
    }

    function assignUserToDepartment($user_id , $department_id){
        $user = User::find($user_id);

        if(is_null(UserDepartment::where('department_id' , $department_id)->where('user_id' , $user_id)->get()->first())) {
            $user->departments()->attach($department_id);
        }else{
            $user->departments()->detach($department_id);
        }
    }
}
