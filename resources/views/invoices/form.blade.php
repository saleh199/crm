@extends('layouts.master')

@section('title' , $pageInfo['page_name'].' - '. $pageInfo['title'])


@section('content')

<div class="row">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($model, [ 'url' => $pageInfo['form_url'] , 'method' => 'post',  'class' => 'col-lg-12', 'files' => true]) !!}
        {{ method_field($pageInfo['form_method']) }}
        {!! Form::token() !!}

        <div class="form-group">
            {!! Form::label('التفاصيل') !!}
            {!! Form::textarea('description', null, ['placeholder' =>'تفاصيل الفاتورة', 'class' => 'form-control', 'id' => 'description']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('تاريخ الإصدار') !!}
            {!! Form::text('issue_date', date('Y-m-d'), ['placeholder' =>'تاريخ المهمة', 'class' => 'form-control date', 'id' => 'issue_date']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('المجموع') !!}
            {!! Form::text('total', null, ['placeholder' =>'0.00', 'class' => 'form-control', 'id' => 'description']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('العملة') !!}
            @if($model)
                {!! Form::select('currency' , \App\Services\InvoiceService::CURRENCIES , $model->currency) !!}
            @else
                {!! Form::select('currency' , \App\Services\InvoiceService::CURRENCIES , old('currency')) !!}
            @endif
        </div>

        <div class="form-group">
            {!! Form::submit( "حفظ", ['class' => 'btn btn-primary']) !!}
            <a href="{{ url('/orders/'.$order->id.'/view') }}" class="btn btn-warning">الغاء الأمر</a>
        </div>
    {!! Form::close() !!}

</div>

<script type="text/javascript">
    $(document).ready(function (){
        $('#issue_date').datepicker({
            format: "yyyy-mm-dd",
            language: "ar",
            rtl: true,
            daysOfWeekDisabled: "5",
            daysOfWeekHighlighted: "5",
            autoclose: true })
    });
</script>
@endsection
