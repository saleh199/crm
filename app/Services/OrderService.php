<?php
/**
 * Created by PhpStorm.
 * User: amro
 * Date: 12/3/16
 * Time: 12:03 PM.
 */

namespace App\Services;

use App\Models\Order;
use Illuminate\Support\Facades\DB;

/**
 * Class OrderService.
 */
class OrderService
{
    public function __construct()
    {
    }

    /**
     * @param $criteria
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getList($criteria = [])
    {
        $res = $this->resolveCriteria($criteria)->get();

        return $res;
    }

    public function getOne($id)
    {
        $res = Order::findOrFail($id);

        return $res;
    }

    public function getCount($criteria = [])
    {
        $res = $this->resolveCriteria($criteria)->count();

        return $res;
    }

    protected function resolveCriteria($data = [])
    {
        $query = Order::Query();

        if (array_key_exists('columns', $data)) {
            $query = $query->select($data['columns']);
        }

        if (array_key_exists('order_id', $data)) {
            $query = $query->where('id', $data['order_id']);
        }

        if (array_key_exists('user_id', $data)) {
            $query = $query->where('user_id', $data['user_id']);
        }

        if (array_key_exists('client_id', $data)) {
            $query = $query->where('client_id', $data['client_id']);
        }

        if (array_key_exists('status', $data)) {
            if(!is_array($data['status'])){
                $data['status'] = [$data['status']];
            }
            $query = $query->whereIn('status', $data['status']);
        }

        if (array_key_exists('year', $data)) {
            $query = $query->where(DB::raw('YEAR(created_at)'), $data['year']);
        }

        if (array_key_exists('title', $data)) {
            $query = $query->where('title', 'LIKE', '%'.$data['title'].'%');
        }

        if (array_key_exists('description', $data)) {
            $query = $query->where('description', 'LIKE', '%'.$data['description'].'%');
        }

        if (array_key_exists('created_at', $data)) {
            $query = $query->where('created_at', 'LIKE', '%'.$data['created_at'].'%');
        }

        if (array_key_exists('limit', $data) && array_key_exists('offset', $data)) {
            $query = $query->take($data['limit']);
            $query = $query->skip($data['offset']);
        }

        if (array_key_exists('group_by', $data)) {
            $query = $query->groupBy($data['group_by']);
        }

        return $query;
    }

    public function create($dataIn, Order &$Order)
    {
        $this->mapDataModel($dataIn, $Order);

        $Order->save();

        return $Order;
    }

    public function mapDataModel($data, Order &$model)
    {
        $attribute = [
            'title',
            'description',
            'status',
            'user_id',
            'client_id',
        ];

        foreach ($attribute as $val) {
            if (array_key_exists($val, $data)) {
                $model->$val = $data[$val];
            }
        }
    }

    public function update($dataIn, Order &$Order)
    {
        $this->mapDataModel($dataIn, $Order);
        $Order->save();

        return $Order;
    }

    public function delete($id)
    {
        $res = $this->getOne($id);

        $res->delete();
    }
}
