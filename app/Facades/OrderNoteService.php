<?php

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class OrderNoteService extends Facade
{
    protected static function getFacadeAccessor() { return 'OrderNoteService'; }
}