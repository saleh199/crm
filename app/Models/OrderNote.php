<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderNote extends Model
{
    function order(){
        return $this->belongsTo('App\Models\Order');
    }
    function user(){
        return $this->belongsTo('App\Models\User');
    }
}
