<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table){
            $table->increments('id');
            $table->string('title' , 45)->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('client_id');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders' , function (Blueprint $table){
            $table->dropForeign(['user_id']);
            $table->dropForeign(['client_id']);
        });
        Schema::dropIfExist('orders');
    }
}

