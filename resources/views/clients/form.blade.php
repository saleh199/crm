@extends('layouts.master')

@section('title' , $pageInfo['page_name'].' - '. $pageInfo['title'])


@section('content')

<div class="row">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($model, [ 'url' => $pageInfo['form_url'] , 'method' => 'post',  'class' => 'col-lg-12', 'files' => true]) !!}
        {{ method_field($pageInfo['form_method']) }}
        {!! Form::token() !!}
        <div class="form-group">
            {!! Form::label('الإسم الكامل') !!}
            {!! Form::text('full_name', null, ['placeholder' =>'الإسم الكامل', 'class' => 'form-control', 'id' => 'full_name']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('البريد الإلكتروني') !!}
            {!! Form::text('email', null, ['placeholder' =>'البريد الإلكتروني', 'class' => 'form-control', 'id' => 'email']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('الهاتف') !!}
            {!! Form::text('phone', null, ['placeholder' =>'الهاتف', 'class' => 'form-control', 'id' => 'phone']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('الجوال') !!}
            {!! Form::text('mobile', null, ['placeholder' =>'الجوال', 'class' => 'form-control', 'id' => 'mobile']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('العنوان') !!}
            {!! Form::text('address', null, ['placeholder' =>'العنوان', 'class' => 'form-control', 'id' => 'address']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('البلد') !!}
            @if($model)
                {!! Form::select('country_id', $countries, $model->country_id, ['class' => 'selectpicker', 'data-live-search'=>'true' ,'placeholder' => 'اختر البلد']) !!}
            @else
                {!! Form::select('country_id', $countries, old('country_id' , null), ['placeholder' => 'اختر البلد']) !!}
            @endif
        </div>
        <div class="form-group">
            {!! Form::label('المدينة') !!}
            {!! Form::text('city', null, ['placeholder' =>'المدينة', 'class' => 'form-control', 'id' => 'city']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('ملاحظات') !!}
            {!! Form::textarea('note', null, ['placeholder' =>'ملاحظات', 'class' => 'form-control', 'id' => 'note']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit( "حفظ", ['class' => 'btn btn-primary']) !!}
            <a href="{{ url('/clients') }}" class="btn btn-warning">الغاء الأمر</a>
        </div>


    {!! Form::close() !!}

</div>

@endsection
