@extends('layouts.master')

@section('title' , $pageInfo['page_name'].' - '. $pageInfo['title'])


@section('content')

<div class="row">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($model, [ 'url' => $pageInfo['form_url'] , 'method' => 'post',  'class' => 'col-lg-12', 'files' => true]) !!}
        {{ method_field($pageInfo['form_method']) }}
        {!! Form::token() !!}

        @can('manage', \App\Models\Order::class)
        <div class="form-group">
            {!! Form::label('المستخدم') !!}

                @if($model)
                    {!! Form::select('user_id', $users, $model->user_id, ['class' => 'selectpicker' , 'data-live-search'=>'true']) !!}
                @else
                    {!! Form::select('user_id', $users, old('user_id' , null), ['class' => 'selectpicker' , 'data-live-search'=>'true']) !!}
                @endif
        </div>
        @endcan

        <div class="form-group">
            {!! Form::label('العميل') !!}

            @if($model)
                {!! Form::select('client_id', $clients, $model->client_id, ['class' => 'selectpicker' , 'data-live-search'=>'true']) !!}
            @else
                {!! Form::select('client_id', $clients, old('client_id' , null), ['class' => 'selectpicker' , 'data-live-search'=>'true']) !!}
            @endif
        </div>


        <div class="form-group">
            {!! Form::label('عنوان الطلب') !!}
            {!! Form::text('title', null, ['placeholder' =>'عنوان الطلب', 'class' => 'form-control', 'id' => 'name']) !!}
        </div>


        <div class="form-group">
            {!! Form::label('حالة الطلب') !!}
            @if($model)
                {!! Form::select('status' , ['0' => 'قيد الإنتظار', '1' => 'قيد التنفيذ' , '2'=> 'نفذ'] , $model->status) !!}
            @else
                {!! Form::select('status' , ['0' => 'قيد الإنتظار', '1' => 'قيد التنفيذ' , '2'=> 'نفذ'] , old('status')) !!}
            @endif
        </div>


        <div class="form-group">
            {!! Form::label('التفاصيل') !!}
            {!! Form::textarea('description', null, ['placeholder' =>'تفاصيل الطلب', 'class' => 'form-control', 'id' => 'description']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit( "حفظ", ['class' => 'btn btn-primary']) !!}
            <a href="{{ url('/orders') }}" class="btn btn-warning">الغاء الأمر</a>
        </div>
    {!! Form::close() !!}

</div>
@endsection
