<?php

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class DepartmentService extends Facade
{
    protected static function getFacadeAccessor() { return 'DepartmentService'; }
}