<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function manage(User $user)
    {
        return $user->isSuperAdmin();
    }

    public function view(User $currentUser, User $user)
    {
        return $currentUser->id == $user->id;
    }
}
