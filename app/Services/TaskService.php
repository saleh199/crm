<?php
/**
 * Created by PhpStorm.
 * User: amro
 * Date: 12/3/16
 * Time: 12:03 PM.
 */

namespace App\Services;

use App\Models\Task;

/**
 * Class TaskService.
 */
class TaskService
{
    /**
     * @param $criteria
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getList($criteria = [])
    {
        $res = $this->resolveCriteria($criteria)->get();

        return $res;
    }

    public function getOne($id)
    {
        $res = Task::findOrFail($id);

        return $res;
    }

    public function getCount($criteria = [])
    {
        $res = $this->resolveCriteria($criteria)->get();

        return $res;
    }

    protected function resolveCriteria($data = [])
    {
        $query = Task::Query();

        if (array_key_exists('columns', $data)) {
            $query = $query->select($data['columns']);
        }

        if (array_key_exists('task_id', $data)) {
            $query = $query->where('id', $data['task_id']);
        }

        if (array_key_exists('user_id', $data)) {
            $query = $query->where('user_id', $data['user_id']);
        }

        if (array_key_exists('issue_date', $data)) {
            $query = $query->where('issue_date', $data['issue_date']);
        }

        if (array_key_exists('status', $data)) {
            $query = $query->where('status', $data['status']);
        }

        if (array_key_exists('name', $data)) {
            $query = $query->where('name', 'LIKE', '%'.$data['name'].'%');
        }
        if (array_key_exists('description', $data)) {
            $query = $query->where('description', 'LIKE', '%'.$data['description'].'%');
        }

        if (array_key_exists('created_at', $data)) {
            $query = $query->where('created_at', 'LIKE', '%'.$data['created_at'].'%');
        }

        if (array_key_exists('limit', $data) && array_key_exists('offset', $data)) {
            $query = $query->take($data['limit']);
            $query = $query->skip($data['offset']);
        }

        if (array_key_exists('group_by', $data)) {
            $query = $query->groupBy($data['group_by']);
        }

        return $query;
    }

    public function create($dataIn, Task &$Task)
    {
        $this->mapDataModel($dataIn, $Task);

        $Task->save();
    }

    public function mapDataModel($data, Task &$model)
    {
        $attribute = [
            'name',
            'description',
            'issue_date',
            'status',
            'user_id',
        ];

        foreach ($attribute as $val) {
            if (array_key_exists($val, $data)) {
                $model->$val = $data[$val];
            }
        }
    }

    public function update($dataIn, Task &$Task)
    {
        $this->mapDataModel($dataIn, $Task);
        $Task->save();
    }

    public function delete($id)
    {
        $res = $this->getOne($id);

        $res->delete();
    }
}
