<?php

namespace App\Http\Controllers;

use App\Facades\DepartmentService;
use App\Http\Requests\DepartmentRequest;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


/**
 * Class DepartmentsController.
 */
class DepartmentsController extends Controller
{

    public function index()
    {
        if (!Auth::user()->can('manage', Department::class)) {
            abort(403);
        }

        $results = DepartmentService::getList();
        $pageInfo['page_name'] = 'إدارة الأقسام';
        $pageInfo['title'] = 'الأقسام';

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('departments_list')],
        ];

        return view('departments.list', compact('results', 'pageInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param null $model
     *
     * @return \Illuminate\Http\Response
     */
    public function create($model = null)
    {
        if (!Auth::user()->can('manage', Department::class)) {
            abort(403);
        }

        $pageInfo['page_name'] = 'إدارة الأقسام';
        $pageInfo['title'] = 'اضافة قسم';

        $pageInfo['form_method'] = 'POST';
        $pageInfo['form_url'] = url('departments');

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('departments_list')],
            ['title' => $pageInfo['title'], 'url' => route('departments_create')],
        ];

        return view('departments.form', compact('pageInfo', 'model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DepartmentRequest|Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentRequest $request)
    {
        if (!Auth::user()->can('manage', Department::class)) {
            abort(403);
        }

        $model = new Department();
        $dataIn = $request->all();
        try {
            $model = DepartmentService::create($dataIn, $model);
        } catch (\Exception $e) {
            return redirect('departments/create')->withInput()->withErrors(trans('all.create-error'));
        }

        return redirect('departments')->with('success', trans('all.success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->can('manage', Department::class)) {
            abort(403);
        }

        $model = DepartmentService::getOne($id);
        $pageInfo['form_method'] = 'PUT';

        $pageInfo['page_name'] = 'إدارة الأقسام';
        $pageInfo['title'] = $model->name;

        $pageInfo['form_url'] = url('departments/'.$id);

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('departments_list')],
            ['title' => $model->name, 'url' => route('departments_edit', ['id' => $model->id])],
            ['title' => 'تعديل', 'url' => route('departments_edit', ['id' => $model->id])],
        ];

        return view('departments.form', compact('pageInfo', 'model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DepartmentRequest|Request $request
     * @param int                       $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentRequest $request, $id)
    {
        if (!Auth::user()->can('manage', Department::class)) {
            abort(403);
        }

        $model = DepartmentService::getOne($id);
        $dataIn = $request->all();
        try {
            $model = DepartmentService::update($dataIn, $model);
        } catch (\Exception $e) {
            return redirect('departments/'.$id.'/edit')->withInput()->withErrors(trans('all.edit-error'));
        }

        return redirect('departments')->with('success', trans('all.success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!Auth::user()->can('manage', Department::class)) {
            abort(403);
        }
        
        $model = DepartmentService::delete($id);

        return redirect('departments')->with('success', trans('all.success'));
    }
}
