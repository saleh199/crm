<?php

namespace App\Http\Controllers;

use App\Facades\ClientService;
use App\Facades\InvoiceService;
use App\Facades\OrderNoteService;
use App\Facades\OrderService;
use App\Facades\UserService;
use App\Http\Requests\InvoiceRequest;
use App\Http\Requests\OrderNoteRequest;
use App\Http\Requests\OrderRequest;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\OrderNote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

/**
 * Class OrdersController.
 */
class OrdersController extends Controller
{
    public function index()
    {
        $filters = [];

        if (!Auth::user()->can('manage', Order::class)) {
            $filters = [
                'user_id' => Auth::user()->id,
            ];
        }

        $results = OrderService::getList($filters);
        $pageInfo['page_name'] = 'إدارة الطلبات';
        $pageInfo['title'] = 'الطلبات';

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('orders_list')],
        ];

        return view('orders.list', compact('results', 'pageInfo'));
    }

    public function view($id)
    {
        $order = OrderService::getOne($id);

        if (!Auth::user()->can('view', $order)) {
            abort(403, 'Unauthorized action.');
        }

        $pageInfo['page_name'] = 'إدارة الطلبات';
        $pageInfo['title'] = $order->title;

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('orders_list')],
            ['title' => $order->title, 'url' => route('orders_view', ['id' => $order->id])],
        ];

        return view('orders.view', compact('order', 'pageInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param null $model
     *
     * @return \Illuminate\Http\Response
     */
    public function create($model = null)
    {
        $pageInfo['page_name'] = 'إدارة الطلبات';
        $pageInfo['title'] = 'اضافة طلب';
        $pageInfo['form_method'] = 'POST';
        $pageInfo['form_url'] = url('orders');

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('orders_list')],
            ['title' => $pageInfo['title'], 'url' => route('orders_create')],
        ];

        $users = UserService::getList();

        $users = $users->mapWithKeys(function ($item, $key) {
            return [strval($item->id).' ' => $item->name];
        })->all();

        $clients = ClientService::getList();

        $clients = $clients->mapWithKeys(function ($item, $key) {
            return [strval($item->id).' ' => $item->full_name];
        })->all();

        return view('orders.form', compact('pageInfo', 'users', 'clients', 'model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrderRequest|Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $model = new Order();
        $dataIn = $request->all();

        if (!Auth::user()->can('manage', Order::class)) {
            $dataIn['user_id'] = Auth::user()->id;
        }

        try {
            $model = OrderService::create($dataIn, $model);
        } catch (\Exception $e) {
            return redirect('orders/create')->withInput()->withErrors(trans('all.create-error'));
        }

        return redirect('orders')->with('success', trans('all.success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->can('manage', Order::class)) {
            abort(403, 'Unauthorized action.');
        }

        $model = OrderService::getOne($id);
        $pageInfo['form_method'] = 'PUT';
        $pageInfo['page_name'] = 'إدارة الطلبات';
        $pageInfo['title'] = $model->title;
        $pageInfo['form_url'] = url('orders/'.$id);

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('orders_list')],
            ['title' => $pageInfo['title'], 'url' => route('orders_view', ['id' => $model->id])],
            ['title' => 'تعديل', 'url' => route('orders_edit', ['id' => $model->id])],
        ];

        $users = UserService::getList();

        $users = $users->mapWithKeys(function ($item, $key) {
            return [strval($item->id).' ' => $item->name];
        })->all();

        $clients = ClientService::getList();

        $clients = $clients->mapWithKeys(function ($item, $key) {
            return [strval($item->id).' ' => $item->full_name];
        })->all();

        return view('orders.form', compact('pageInfo', 'users', 'clients', 'model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrderRequest|Request $request
     * @param int                  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, $id)
    {
        if (!Auth::user()->can('manage', Order::class)) {
            abort(403, 'Unauthorized action.');
        }

        $model = OrderService::getOne($id);
        $dataIn = $request->all();
        try {
            $model = OrderService::update($dataIn, $model);
        } catch (\Exception $e) {
            return redirect('orders/'.$id.'/edit')->withInput()->withErrors(trans('all.edit-error'));
        }

        return redirect('orders/'.$model->id.'/view')->with('success', trans('all.success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!Auth::user()->can('manage', Order::class)) {
            abort(403, 'Unauthorized action.');
        }

        $model = OrderService::delete($id);

        return redirect('orders')->with('success', trans('all.success'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $order_id
     * @param null $model
     *
     * @return \Illuminate\Http\Response
     */
    public function invoiceCreate($order_id, $model = null)
    {
        $order = OrderService::getOne($order_id);

        if (!Auth::user()->can('view', $order)) {
            abort(403, 'Unauthorized action.');
        }

        $pageInfo['page_name'] = 'إدارة الطلبات';
        $pageInfo['title'] = 'فوترة الطلب '.$order->title;
        $pageInfo['form_method'] = 'POST';
        $pageInfo['form_url'] = url('orders/'.$order->id.'/invoice/create');

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('orders_list')],
            ['title' => $order->title, 'url' => route('orders_view', ['id' => $order->id])],
            ['title' => 'أضافة فاتورة', 'url' => route('invoices_create', ['order_id' => $order->id])],
        ];

        return view('invoices.form', compact('pageInfo', 'order', 'model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param InvoiceRequest|Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function invoiceStore(InvoiceRequest $request, $order_id)
    {
        $order = OrderService::getOne($order_id);

        if (!Auth::user()->can('view', $order)) {
            abort(403, 'Unauthorized action.');
        }

        $model = new Invoice();
        $dataIn = $request->all();
        $dataIn['order_id'] = $order->id;
        $dataIn['user_id'] = Auth::id();
        try {
            $model = InvoiceService::create($dataIn, $model);
        } catch (\Exception $e) {
            dd($e);

            return redirect('orders/'.$order->id.'/invoice/create')->withInput()->withErrors(trans('all.create-error'));
        }

        return redirect('orders/'.$order->id.'/view')->with('success', trans('all.success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $order_id
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function invoiceEdit($order_id, $id)
    {
        $order = OrderService::getOne($order_id);

        if (!Auth::user()->can('view', $order)) {
            abort(403, 'Unauthorized action.');
        }

        $pageInfo['page_name'] = 'إدارة الطلبات';
        $pageInfo['title'] = 'تعديل فاتورة الطلب '.$order->title;

        $model = InvoiceService::getOne($id);
        $pageInfo['form_method'] = 'PUT';

        $pageInfo['form_url'] = url('orders/'.$order->id.'/invoice/'.$model->id);

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('orders_list')],
            ['title' => $order->title, 'url' => route('orders_view', ['id' => $order->id])],
            ['title' => 'فاتورة الطلب', 'url' => route('invoices_edit', ['order_id' => $order->id, 'id' => $model->id])],
        ];

        return view('invoices.form', compact('pageInfo', 'order', 'model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param InvoiceRequest|Request $request
     * @param int                    $id
     *
     * @return \Illuminate\Http\Response
     */
    public function invoiceUpdate(InvoiceRequest $request, $order_id, $id)
    {
        $order = OrderService::getOne($order_id);

        if (!Auth::user()->can('view', $order)) {
            abort(403, 'Unauthorized action.');
        }

        $model = InvoiceService::getOne($id);
        $dataIn = $request->all();
        $dataIn['user_id'] = Auth::id();
        try {
            $model = InvoiceService::update($dataIn, $model);
        } catch (\Exception $e) {
            return redirect()->withInput('orders/'.$order->id.'/invoice/'.$model->id)->withErrors(trans('all.edit-error'));
        }

        return redirect('orders/'.$order->id.'/view')->with('success', trans('all.success'));
    }

    public function noteStore(OrderNoteRequest $request, $order_id)
    {
        $order = OrderService::getOne($order_id);

        if (!Auth::user()->can('view', $order)) {
            abort(403, 'Unauthorized action.');
        }

        $model = new OrderNote();
        $dataIn = $request->all();
        $dataIn['order_id'] = $order->id;
        $dataIn['user_id'] = Auth::id();
        try {
            $model = OrderNoteService::create($dataIn, $model);
        } catch (\Exception $e) {
            return redirect('orders/'.$order->id.'/view')->withInput()->withErrors(trans('all.create-error'));
        }

        return redirect('orders/'.$order->id.'/view')->with('success', trans('all.success'));
    }
}
