<?php

namespace App\Http\Controllers;

use App\Facades\DepartmentService;
use App\Facades\UserService;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class UsersController.
 */
class UsersController extends Controller
{
    public function index()
    {
        if (!Auth::user()->can('manage', User::class)) {
            abort(403);
        }

        $results = UserService::getList();
        $pageInfo['page_name'] = 'إدارة المستخدمين';
        $pageInfo['title'] = 'المستخدمين';

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('users_list')],
        ];

        return view('users.list', compact('results', 'pageInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param null $model
     *
     * @return \Illuminate\Http\Response
     */
    public function create($model = null)
    {
        if (!Auth::user()->can('manage', User::class)) {
            abort(403);
        }

        $pageInfo['page_name'] = 'إدارة المستخدمين';
        $pageInfo['title'] = 'اضافة مستخدم';

        $pageInfo['form_method'] = 'POST';
        $pageInfo['form_url'] = url('users');

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('users_list')],
            ['title' => $pageInfo['title'], 'url' => route('users_create')],
        ];

        return view('users.form', compact('pageInfo', 'model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest|Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if (!Auth::user()->can('manage', User::class)) {
            abort(403);
        }

        $model = new User();
        $dataIn = $request->all();
        try {
            $model = UserService::create($dataIn, $model);
        } catch (\Exception $e) {
            return redirect('users/create')->withInput()->withErrors(trans('all.create-error'));
        }

        return redirect('users')->with('success', trans('all.success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = UserService::getOne($id);

        if (!Auth::user()->can('view', $model)) {
            abort(403);
        }

        $pageInfo['form_method'] = 'PUT';

        $pageInfo['page_name'] = 'إدارة المستخدمين';
        $pageInfo['title'] = $model->name;

        $pageInfo['form_url'] = url('users/'.$id);

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('users_list')],
            ['title' => $pageInfo['title'], 'url' => route('users_view', ['id' => $model->id])],
            ['title' => 'تعديل', 'url' => route('users_edit', ['id' => $model->id])],
        ];

        return view('users.form', compact('pageInfo', 'model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $model = UserService::getOne($id);

        if (!Auth::user()->can('view', $model)) {
            abort(403);
        }

        $dataIn = $request->all();
        try {
            $model = UserService::update($dataIn, $model);
        } catch (\Exception $e) {
            return redirect('users/'.$id.'/edit')->withInput()->withErrors(trans('all.edit-error'));
        }

        return redirect('users')->with('success', trans('all.success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!Auth::user()->can('manage', User::class)) {
            abort(403);
        }

        $model = UserService::delete($id);

        return redirect('users')->with('success', trans('all.success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $model = UserService::getOne($id);

        if (!Auth::user()->can('view', $model)) {
            abort(403);
        }

        $departments = DepartmentService::getList();
        $userDepartments = $model->departments->pluck('id')->toArray();

        $pageInfo['form_method'] = 'POST';

        $pageInfo['page_name'] = 'إدارة المستخدمين';
        $pageInfo['title'] = $model->name;

        $pageInfo['form_url'] = url('users/'.$id.'/department');

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('users_list')],
            ['title' => $pageInfo['title'], 'url' => route('users_view', ['id' => $model->id])],
        ];

        return view('users.view', compact('pageInfo', 'departments', 'userDepartments', 'model'));
    }

    public function departments(Request $request, $id, $department_id = null)
    {
        if (!Auth::user()->can('manage', User::class)) {
            abort(403);
        }

        if (is_null($department_id)) {
            $department_id = $request->get('department_id');
        }

        $user = UserService::getOne($id);
        $department = DepartmentService::getOne($department_id);
        try {
            DepartmentService::assignUserToDepartment($id, $department_id);
        } catch (\Exception $e) {
            return redirect('users/'.$id.'/view')->withInput()->withErrors(trans('all.edit-error'));
        }

        return redirect('users/'.$id.'/view')->with('success', trans('all.success'));
    }
}
