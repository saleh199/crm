<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    function user(){
        return $this->belongsTo('App\Models\User');
    }

    function notes(){
        return $this->hasMany('App\Models\TaskNote');
    }
}
