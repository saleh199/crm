<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;
use App\Services\UserService;

class CrmServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('UserService', function() {
            return new UserService();
        });
        App::bind('ClientService', function() {
            return new App\Services\ClientService();
        });
        App::bind('CountryService', function() {
                return new App\Services\CountryService();
            });
        App::bind('DepartmentService', function() {
                return new App\Services\DepartmentService();
            });
        App::bind('InvoiceService', function() {
                return new App\Services\InvoiceService();
            });
        App::bind('OrderNoteService', function() {
                return new App\Services\OrderNoteService();
            });
        App::bind('OrderService', function() {
                return new App\Services\OrderService();
            });
        App::bind('TaskService', function() {
                return new App\Services\TaskService();
            });
        App::bind('TaskNoteService', function() {
                return new App\Services\TaskNoteService();
            });
    }

    public function provides()
    {
        return [
                'UserService' ,
                'ClientService' ,
                'CountryService' ,
                'DepartmentService' ,
                'OrderNoteService' ,
                'OrderService' ,
                'TaskService' ,
                'TaskNoteService'
        ];
    }
}
