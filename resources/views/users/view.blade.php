@extends('layouts.master')

@section('title' , $pageInfo['page_name'].' - '. $pageInfo['title'])


@section('content')

<div class="row">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        @if (session('success'))
            <div class="alert alert-success alert-dismissable">
                <ul>
                    <li>{{ session('success') }}</li>
                </ul>
            </div>
        @endif
</div>

<div class="row">
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $model->name }}  <small>اخر تعديل:  {{ $model->updated_at }}</small>
            </div>
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th width="60">الرقم</th>
                        <td> {{ $model->id }}</td>
                    </tr>
                    <tr>
                        <th>اسم المستخدم</th>
                        <td>{{ $model->name }}</td>
                    </tr>
                    <tr>
                        <th>البريد الإلكتورني</th>
                        <td> {{ $model->email}}</td>
                    </tr>
                    <tr>
                        <th>الأقسام</th>
                        <td>
                            @if($model->departments->count() > 0)
                                @foreach($model->departments as $value)
                                    <span class="btn btn-info ">
                                            {{ $value->name }} &nbsp;&nbsp;&nbsp;&nbsp;
                                        @can('manage', \App\Models\User::class)
                                        <a href="{{ url('/users/'.$model->id.'/department/'.$value->id) }}" class="text-danger"> <i class="fa fa-times" aria-hidden="true"></i></a>
                                        @endcan
                                    </span>
                                @endforeach
                            @endif
                        </td>
                    </tr>
                </table>
                <a href="{{ url('users/'.$model->id.'/edit') }}" class="btn btn-primary ">تعديل </a>
            </div>
        </div>
    </div>
    @can('manage', \App\Models\User::class)
    <div class="col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading">ادارة اقسام المستخدم</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12">
                        {!! Form::open([ 'url' => url('users/'.$model->id.'/department'), 'method' => 'post',  'class' => 'col-lg-12', 'files' => true]) !!}
                        {{ method_field('POST') }}
                        {!! Form::token() !!}
                        <div class="form-group">
                            {!! Form::label('القسم') !!}
                            <select name="department_id" class="selectpicker" data-live-search="true">
                                @foreach($departments as $val)
                                    @if(!in_array($val->id, $userDepartments))
                                        <option value="{{ $val->id }}"> {{ $val->name }} </option>
                                    @endif
                                @endforeach
                            </select>

                        </div>
                        <div class="form-group">
                            {!! Form::submit( "إضافة", ['class' => 'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan
</div>
@endsection
