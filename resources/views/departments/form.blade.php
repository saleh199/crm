@extends('layouts.master')

@section('title' , $pageInfo['page_name'].' - '. $pageInfo['title'])


@section('content')

<div class="row">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($model, [ 'url' => $pageInfo['form_url'] , 'method' => 'post',  'class' => 'col-lg-12', 'files' => true]) !!}
        {{ method_field($pageInfo['form_method']) }}
        {!! Form::token() !!}
        <div class="form-group">
            {!! Form::label('الإسم') !!}
            {!! Form::text('name', null, ['placeholder' =>'الإسم', 'class' => 'form-control', 'id' => 'name']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('وصف القسم') !!}
            {!! Form::textarea('description', null, ['placeholder' =>'الوصف', 'class' => 'form-control', 'id' => 'description']) !!}
        </div>

        <div class="form-group">
            {!! Form::label(' إدارة ؟') !!}<br>
            {!! Form::radio('is_admin', 1) !!} نعم
            <br>
            {!! Form::radio('is_admin', 0) !!} لا
        </div>
        <div class="form-group">
            {!! Form::submit( "حفظ", ['class' => 'btn btn-primary']) !!}
            <a href="{{ url('/departments') }}" class="btn btn-warning">الغاء الأمر</a>
        </div>


    {!! Form::close() !!}

</div>

@endsection
