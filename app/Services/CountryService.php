<?php
/**
 * Created by PhpStorm.
 * User: amro
 * Date: 12/3/16
 * Time: 12:03 PM
 */
namespace App\Services;

use App\Models\Country;

/**
 * Class CountryService
 * @package App\Services
 */
class CountryService{


    function __construct()
    {
    }

    /**
     * @param $criteria
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    function getList($criteria = []){
        $res = $this->resolveCriteria($criteria)->get();
        return $res;
    }


    function getOne($id){
        $res = Country::findOrFail($id);
        return $res;
    }
    protected function resolveCriteria($data = [])
    {
        $query = Country::Query();

        if (array_key_exists('columns', $data)) {
            $query = $query->select($data['columns']);
        }

        if (array_key_exists('country_id', $data)) {
            $query = $query->where('id', $data['country_id']);
        }

        if (array_key_exists('name', $data)) {
            $query = $query->where('name', 'LIKE', "%" . $data['name'] . "%");
        }

        if (array_key_exists('created_at', $data)) {
            $query = $query->where('created_at', "LIKE", "%".$data['created_at']."%");
        }

        if (array_key_exists('limit', $data) && array_key_exists('offset', $data)) {
            $query = $query->take($data['limit']);
            $query = $query->skip($data['offset']);
        }

        $query->orderBy('name' , 'asc');

        return $query;
    }

    function create( $dataIn = [], Country &$Country){
        $this->mapDataModel($dataIn , $Country);

        $Country->save();
    }

    public function mapDataModel($data, Country &$model)
    {
        $attribute = [
            'name',
        ];

        foreach ($attribute as $val) {
            if (array_key_exists($val, $data)) {
                $model->$val = $data[$val];
            }
        }
    }

    function update($dataIn = [] , Country &$Country ){

        $this->mapDataModel($dataIn , $Country);
        $Country->save();
    }

    function delete($id){

        $res = $this->getOne($id);

        $res->delete();
    }


}
