<?php
/**
 * Created by PhpStorm.
 * User: amro
 * Date: 12/3/16
 * Time: 12:03 PM
 */
namespace App\Services;

use App\Models\Client;

/**
 * Class ClientService
 * @package App\Services
 */
class ClientService{


    function __construct()
    {
    }

    /**
     * @param $criteria
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    function getList($criteria = []){
        $res = $this->resolveCriteria($criteria)->get();
        return $res;
    }


    function getOne($id){
        $res = Client::findOrFail($id);
        return $res;
    }
    protected function resolveCriteria($data = [])
    {
        $query = Client::Query();

        if (array_key_exists('columns', $data)) {
            $query = $query->select($data['columns']);
        }

        if (array_key_exists('client_id', $data)) {
            $query = $query->where('id', $data['client_id']);
        }


        if (array_key_exists('full_name', $data)) {
            $query = $query->where('full_name', 'LIKE', "%" . $data['full_name'] . "%");
        }

        if (array_key_exists('email', $data)) {
            $query = $query->where('email', 'LIKE'  , "%" . $data['email'] ."%");
        }

        if (array_key_exists('phone', $data)) {
            $query = $query->where('phone', 'LIKE'  , "%" . $data['phone'] ."%");
        }

        if (array_key_exists('country_id', $data)) {
            $query = $query->where('country_id', $data['country_id']);
        }


        if (array_key_exists('created_at', $data)) {
            $query = $query->where('created_at', "LIKE", "%".$data['created_at']."%");
        }

        if (array_key_exists('limit', $data) && array_key_exists('offset', $data)) {
            $query = $query->take($data['limit']);
            $query = $query->skip($data['offset']);
        }

        return $query;
    }

    function create( $dataIn = [], Client &$Client){
        $this->mapDataModel($dataIn , $Client);

        $Client->save();
    }

    public function mapDataModel($data, Client &$model)
    {
        $attribute = [
            'full_name',
            'email',
            'phone',
            'mobile',
            'address',
            'note',
            'country_id',
            'city',
        ];

        foreach ($attribute as $val) {
            if (array_key_exists($val, $data)) {
                $model->$val = $data[$val];
            }
        }
    }

    function update($dataIn = [] , Client &$Client ){

        $this->mapDataModel($dataIn , $Client);
        $Client->save();
    }

    function delete($id){

        $res = $this->getOne($id);

        $res->delete();
    }


}
