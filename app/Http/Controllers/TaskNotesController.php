<?php

namespace App\Http\Controllers;

use App\Facades\TaskNoteService;
use App\Facades\TaskService;
use App\Http\Requests\TaskNoteRequest;
use App\Models\TaskNote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class TasksController
 * @package App\Http\Controllers\Admin
 */
class TaskNotesController extends Controller
{
    /**
     * @param $task_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function index($task_id)
    {
        $task = TaskService::getOne($task_id);

        if(!Auth::user()->can('view', $task)){
            abort(403, 'Unauthorized action.');
        }

        $results =  TaskNoteService::getList(['task_id']);

        $pageInfo['page_name'] = "إدارة الملاحظات للمهمة " . $task->name;
        $pageInfo['title'] = "الملاحظات";

        $pageInfo['breadcrumbs'] = [
            ['title' => "إدارة المهمات", 'url' => route('tasks_list')],
            ['title' => $task->name, 'url' => route('tasks_view', ['id' => $task->id])],
            ['title' => $pageInfo['title'], 'url' => route('tasks_notes_list', ['task_id' => $task->id])],
        ];

        return view( 'tasks.notes.list' , compact(  'task' , 'results', 'pageInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $task_id
     * @param null $model
     * @return \Illuminate\Http\Response
     */
    public function create( $task_id ,  $model = null)
    {
        $task = TaskService::getOne($task_id);

        if(!Auth::user()->can('view', $task)){
            abort(403, 'Unauthorized action.');
        }

        $pageInfo['page_name'] = "إدارة الملاحظات للمهمة " . $task->name;
        $pageInfo['title'] = "اضافة ملاحظة";
        $pageInfo['form_method'] = 'POST';
        $pageInfo['form_url'] = url('tasks/'.$task->id.'/notes');

        $pageInfo['breadcrumbs'] = [
            ['title' => "إدارة المهمات", 'url' => route('tasks_list')],
            ['title' => $task->name, 'url' => route('tasks_view', ['task_id' => $task->id])],
            ['title' => 'الملاحظات', 'url' => route('tasks_notes_list', ['task_id' => $task->id])],
            ['title' => $pageInfo['title'], 'url' => route('tasks_notes_create', ['task_id' => $task->id])],
        ];

        return view('tasks.notes.form' , compact('pageInfo', 'task' , 'model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskNoteRequest|Request $request
     * @param $task_id
     * @return \Illuminate\Http\Response
     */
    public function store(TaskNoteRequest $request , $task_id )
    {
        $task = TaskService::getOne($task_id);

        if(!Auth::user()->can('view', $task)){
            abort(403, 'Unauthorized action.');
        }

        $model = New TaskNote();
        $dataIn = $request->all();
        $dataIn['task_id'] = $task->id;
        $dataIn['user_id'] = Auth::id();
        try {
            $model = TaskNoteService::create($dataIn, $model);
        }catch (\Exception $e) {
            return redirect('tasks/'.$task->id.'/notes/create')->withInput()->withErrors(trans("all.create-error"));
        }
        return redirect('tasks/'.$task->id.'/notes')->with('success' , trans('all.success'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $task_id
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($task_id , $id)
    {
        $task = TaskService::getOne($task_id);

        if(!Auth::user()->can('view', $task)){
            abort(403, 'Unauthorized action.');
        }

        $model = TaskNoteService::getOne($id);
        $pageInfo['form_method'] = 'PUT';
        $pageInfo['page_name'] = "إدارة الملاحظات للمهمة " . $task->name;
        $pageInfo['title'] = "تعديل ملاحظة";

        $pageInfo['form_url'] = url('tasks/'.$task->id.'/notes/'.$id);

        $pageInfo['breadcrumbs'] = [
            ['title' => "إدارة المهمات", 'url' => route('tasks_list')],
            ['title' => $task->name, 'url' => route('tasks_view', ['task_id' => $task->id])],
            ['title' => 'الملاحظات', 'url' => route('tasks_notes_list', ['task_id' => $task->id])],
            ['title' => $pageInfo['title'], 'url' => route('tasks_notes_edit', ['task_id' => $task->id, 'id' => $model->id])],
        ];

        return view('tasks.notes.form' , compact( 'pageInfo' , 'task' , 'model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaskNoteRequest|Request $request
     * @param $task_id
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TaskNoteRequest $request,$task_id,  $id)
    {
        $task = TaskService::getOne($task_id);

        if(!Auth::user()->can('view', $task)){
            abort(403, 'Unauthorized action.');
        }

        $model = TaskNoteService::getOne($id);
        $dataIn = $request->all();
        try {
            $model = TaskNoteService::update($dataIn, $model);
        }catch (\Exception $e) {
            return redirect('tasks/'.$task->id.'/notes/'.$id.'/edit')->withInput()->withErrors(trans("all.edit-error"));
        }
        return redirect('tasks/'.$task->id.'/notes')->with('success' , trans('all.success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $task_id
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $task_id , $id)
    {
        $task = TaskService::getOne($task_id);

        if(!Auth::user()->can('manage', $task)){
            abort(403, 'Unauthorized action.');
        }

        $model = TaskNoteService::delete($id);
        return redirect('tasks/'.$task->id.'/notes')->with('success' , trans('all.success'));
    }
}
