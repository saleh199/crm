@extends('layouts.master')

@section('title' , $pageInfo['page_name'].' - '. $pageInfo['title'])


@section('content')
<div class="row">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissable">
                <ul>
                    <li>{{ session('success') }}</li>
                </ul>
            </div>
        @endif
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <a href="{{ url('orders/create') }}" class="btn btn-primary">إضافة طلب </a>
    </div>
    <div class="panel-body">
        <table width="100%" class="table table-striped table-bordered table-hover" id="model-table">
            <thead>
            <tr>
                <th width="60">الرقم</th>
                <th>عنوان الطلب</th>
                <th>المستخدم</th>
                <th>العميل</th>
                <th>اخر تعديل</th>
                <th>الحالة</th>
                <th>التفاصيل</th>
                <th>الملاحظات</th>
                @can('manage', \App\Models\Order::class)
                <th width="60" >تعديل</th>
                <th width="60" >حذف</th>
                @endcan
            </tr>
            </thead>
            <tbody>
            @php
                $i = 0;
            @endphp
            @if(isset($results))
                @foreach($results as $value)
                    @php
                        $i++;
                    @endphp
            <tr class="{{ ($i++ % 2 ==0)?"odd": "even" }} gradeX">
                <td class="center ">{{ $value->id }}</td>
                <td>{{ $value->title }}</td>
                <td>@if($value->user)
                        {{ $value->user->name }}
                    @else
                        غير محدد
                    @endif
                </td>
                <td>{{ $value->client->full_name }}</td>
                <td>{{ $value->updated_at }}</td>
                <td> @php
                        switch ($value->status ){
                        case 0 :
                        {
                            print  '<i class="fa fa-circle text-danger" aria-hidden="true"></i> قيد الإنتظار';
                            break;
                        }
                        case 1:
                        {
                            print '<i class="fa fa-circle text-info" aria-hidden="true"></i> قيد التنفيذ';
                            break;
                        }
                        case 2:
                        {
                           print '<i class="fa fa-circle text-success" aria-hidden="true"></i> نفذ';
                            break;
                        }
                       }
                     @endphp
                         </td>
                <td align="center" > <a href="{{ url('orders/'.$value->id.'/view') }}" class="text-info">التفاصيل
                    </a></td>
                <td align="center" > <a href="{{ url('orders/'.$value->id.'/notes') }}" class="text-info">الملاحظات
                    </a></td>
                @can('manage', \App\Models\Order::class)
                <td align="center" > <a href="{{ url('orders/'.$value->id.'/edit') }}" class="text-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a></td>
                <td align="center" >

                    <a  data-id="{{ $value->id  }}"  data-title="{!! $value->name !!}" data-url="{{ action('OrdersController@destroy', ['id'=>$value->id]) }}"
                       class="text-danger delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="حذف">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </a>
                </td>
                @endcan
            </tr>
                @endforeach
            @endif

            </tbody>
        </table>
    </div>
</div>


<div class="modal slide-down fade" id="modal-delete">
    <form id="delete-form" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        {{ method_field('DELETE') }}
        <div class="modal-dialog">
            <div class="v-cell">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ trans('all.button_close') }}</span></button>
                        <h4 class="modal-title" id="modal-delete-project-title">حذف <span id="title"></span></h4>
                    </div>
                    <div class="modal-body" id="modal-delete-project-body">
                        هذه العملية لا يمكن التراجع عنها, هل تريد بالتأكيد المتابعة?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">اغلاق</button>
                        <button type="submit" class="btn btn-danger" id="modal-delete-project-button-submit">حذف</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>



<!-- Morris Charts JavaScript -->
<script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-responsive/dataTables.responsive.js') }}"></script>

<!-- Custom Theme JavaScript -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('#model-table').DataTable({
                responsive: true
                @if(App::getLocale() == 'ar')
                ,
                "language":{
                    "url" : "//cdn.datatables.net/plug-ins/1.10.13/i18n/Arabic.json"
                }
                @endif
            });

            $('.delete').click(function(){
                var title = $(this).data('title');
                var deleteUrl = $(this).data('url');
                $('#delete-form').attr('action' , deleteUrl);
                $('span#title').html(title);
                $('#modal-delete').modal({show: true});
            })
        });

    </script>

@endsection
