<?php

namespace App\Providers;

use App\Models\Client;
use App\Models\Department;
use App\Models\Order;
use App\Models\Task;
use App\Models\User;
use App\Policies\ClientPolicy;
use App\Policies\DepartmentPolicy;
use App\Policies\OrderPolicy;
use App\Policies\TaskPolicy;
use App\Policies\UserPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Task::class => TaskPolicy::class,
        Order::class => OrderPolicy::class,
        Client::class => ClientPolicy::class,
        Department::class => DepartmentPolicy::class,
        User::class => UserPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
