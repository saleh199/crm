@if(Auth::check() && isset($pageInfo))
    @if(isset($pageInfo['breadcrumbs']) && $pageInfo['breadcrumbs'] !== false)
    <div class="row">
        <div class="col-ls-12" style="margin-top: 20px;">
            {!! Breadcrumbs::render('general', (is_array($pageInfo['breadcrumbs']) ? $pageInfo['breadcrumbs'] : [])) !!}
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{  $pageInfo['page_name'] }}</h1>
            @if(isset($pageInfo['title']))
            <h2 class="page-header-subtitle"> {{ $pageInfo['title'] }}</h2>
            @endif
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endif