<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    function country(){
        return $this->belongsTo('App\Models\Country');
    }
}
