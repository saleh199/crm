<?php

namespace App\Policies;

use App\Models\Task;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function manage(User $user)
    {
        return $user->isSuperAdmin();
    }

    public function view(User $user, Task $task){
        return $user->id == $task->user_id;
    }
}
