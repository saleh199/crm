<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks' , function (Blueprint $table){
            $table->increments('id');
            $table->string('name' , 45);
            $table->text('description');
            $table->date('issue_date');
            $table->tinyInteger('status')->default(0);
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks' , function (Blueprint $table){
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExist('tasks');
    }
}
