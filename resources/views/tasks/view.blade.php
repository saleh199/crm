@extends('layouts.master')

@section('title' , $pageInfo['page_name'].' - '. $pageInfo['title'])


@section('content')
<div class="row">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissable">
                <ul>
                    <li>{{ session('success') }}</li>
                </ul>
            </div>
        @endif
</div>
<div class="row">
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                    {{ $task->name }}  <small>اخر تعديل:  {{ $task->updated_at }}</small>
            </div>
            <div class="panel-body">

                <table width="100%" class="table table-striped table-bordered table-hover" >
                    <tr>
                        <th width="60">الرقم</th>
                        <td> {{ $task->id }}</td>
                    </tr>
                    <tr>
                        <th>المستخدم</th>
                        <td>@if($task->user)
                                {{ $task->user->name }}
                            @else
                                غير محدد
                            @endif</td>
                    </tr>
                    <tr>
                        <th>الحالة</th>
                        <td> @php
                                switch ($task->status ){
                                  case 0 :
                                    {
                                        print  '<i class="fa fa-circle text-danger" aria-hidden="true"></i> قيد الإنتظار';
                                        break;
                                    }
                                    case 1:
                                    {
                                        print '<i class="fa fa-circle text-info" aria-hidden="true"></i> قيد التنفيذ';
                                        break;
                                    }
                                    case 2:
                                    {
                                       print '<i class="fa fa-circle text-success" aria-hidden="true"></i> نفذت';
                                        break;
                                    }
                               }
                            @endphp</td>
                    </tr>
                    <tr>
                        <th>تاريخ التنفيذ</th>
                        <td>{{ $task->issue_date }}</td>
                    </tr>

                    <tr>
                        <th>التفاصيل</th>
                        <td>{{ $task->description }}</td>
                    </tr>
                </table>
                @can('manage', \App\Models\Task::class)
                <a href="{{ url('tasks/'.$task->id.'/edit') }}" class="btn btn-primary pull-left">تعديل </a>
                @endcan
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('tasks/'.$task->id.'/notes') }}"> الملاحظات </a>
            </div>
            <div class="panel-body">
                @if(!$task->notes->isEmpty())
                    @foreach($task->notes as $value)
                        <div class="well well-sm">
                            <h4>{{ $value->user->name  }}<small> {{ $value->updated_at }} </small></h4>
                            <p>
                                {{ $value->note_text }}
                            </p>
                        </div>
                    @endforeach
                @endif
                <div class="row">
                    <div class="col-xs-12">
                        {!! Form::model(New \App\Models\TaskNote(), [ 'url' => url('tasks/'.$task->id.'/note'), 'method' => 'post',  'class' => 'col-lg-12', 'files' => true]) !!}
                        {{ method_field('post') }}
                        {!! Form::token() !!}

                        <div class="form-group">
                            {!! Form::textarea('note_text', null, ['placeholder' =>'الملاحظة', 'class' => 'form-control', 'id' => 'note_text']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit( "إضافة", ['class' => 'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
