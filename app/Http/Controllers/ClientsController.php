<?php

namespace App\Http\Controllers;

use App\Facades\ClientService;
use App\Facades\CountryService;
use App\Http\Requests\ClientRequest;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class ClientsController.
 */
class ClientsController extends Controller
{
    public function index()
    {
        $results = ClientService::getList();
        $pageInfo['page_name'] = 'إدارة العملاء';
        $pageInfo['title'] = 'العملاء';

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('clients_list')],
        ];

        return view('clients.list', compact('results', 'pageInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param null $model
     *
     * @return \Illuminate\Http\Response
     */
    public function create($model = null)
    {
        $pageInfo['page_name'] = 'إدارة العملاء';
        $pageInfo['title'] = 'اضافة عميل';
        $pageInfo['form_method'] = 'POST';
        $pageInfo['form_url'] = url('clients');

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('clients_list')],
            ['title' => $pageInfo['title'], 'url' => route('clients_create')],
        ];

        $countries = CountryService::getList();
        $countries = $countries->mapWithKeys(function ($item, $key) {
            return [$item->id.' ' => $item->name];
        })->all();

        return view('clients.form', compact('pageInfo', 'countries', 'model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClientRequest|Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        $model = new Client();
        $dataIn = $request->all();
        try {
            $model = ClientService::create($dataIn, $model);
        } catch (\Exception $e) {
            return redirect('clients/create')->withInput()->withErrors(trans('all.create-error'));
        }

        return redirect('clients')->with('success', trans('all.success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = ClientService::getOne($id);
        $pageInfo['form_method'] = 'PUT';
        $pageInfo['page_name'] = 'إدارة العملاء';
        $pageInfo['title'] = $model->full_name;
        $pageInfo['form_url'] = url('clients/'.$id);

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('clients_list')],
            ['title' => $pageInfo['title'], 'url' => route('clients_edit', ['id' => $model->id])],
            ['title' => 'تعديل', 'url' => route('clients_edit', ['id' => $model->id])],
        ];

        $countries = CountryService::getList();
        $countries = $countries->mapWithKeys(function ($item, $key) {
            return [$item->id.' ' => $item->name];
        })->all();

        return view('clients.form', compact('pageInfo', 'countries', 'model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClientRequest|Request $request
     * @param int                   $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ClientRequest $request, $id)
    {
        $model = ClientService::getOne($id);
        $dataIn = $request->all();
        try {
            $model = ClientService::update($dataIn, $model);
        } catch (\Exception $e) {
            return redirect('clients/'.$id.'/edit')->withInput()->withErrors(trans('all.edit-error'));
        }

        return redirect('clients')->with('success', trans('all.success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!Auth::user()->can('manage', Client::class)) {
            abort(403);
        }

        $model = ClientService::delete($id);

        return redirect('clients')->with('success', trans('all.success'));
    }
}
