<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\Models\Users' ,'user_departments' , 'user_id' , 'department_id');
    }
}
