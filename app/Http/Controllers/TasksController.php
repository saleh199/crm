<?php

namespace App\Http\Controllers;

use App\Facades\TaskNoteService;
use App\Facades\TaskService;
use App\Facades\UserService;
use App\Http\Requests\TaskNoteRequest;
use App\Http\Requests\TaskRequest;
use App\Models\Task;
use App\Models\TaskNote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

/**
 * Class TasksController
 * @package App\Http\Controllers\Admin
 */
class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    public function index()
    {
        $filters = [];

        if(!Auth::user()->can('manage', Task::class)){
            $filters = [
                'user_id' => Auth::user()->id
            ];
        }

        $results = TaskService::getList($filters);
        $pageInfo['page_name'] = "إدارة المهمات";
        $pageInfo['title'] = "المهمات";
        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('tasks_list')]
        ];
        
        return view( 'tasks.list' , compact( 'results', 'pageInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param null $model
     * @return \Illuminate\Http\Response
     */
    public function create( $model = null)
    {
        if(!Auth::user()->can('manage', Task::class)){
            abort(403, 'Unauthorized action.');
        }

        $pageInfo['page_name'] = "إدارة المهمات";
            $pageInfo['title'] = "اضافة مهمة";
        $pageInfo['form_method'] = 'POST';
        $pageInfo['form_url'] = url('tasks');

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('tasks_list')],
            ['title' => $pageInfo['title'], 'url' => route('tasks_create')],
        ];

        $users = UserService::getList();

        $users = $users->mapWithKeys(function ($item , $key){
           return ([ strval($item->id)." "=> $item->name]);
         })->all();

        return view('tasks.form' , compact('pageInfo', 'users' , 'model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskRequest $request )
    {
        if(!Auth::user()->can('manage', Task::class)){
            abort(403, 'Unauthorized action.');
        }

        $model = New Task();
        $dataIn = $request->all();
        try {
            $model = TaskService::create($dataIn, $model);
        }catch (\Exception $e) {
            return redirect('tasks/create')->withInput()->withErrors(trans("all.create-error"));
        }
        return redirect('tasks')->with('success' , trans('all.success'));
    }

    public function view($id)
    {
        $task = TaskService::getOne($id);

        if(!Auth::user()->can('view', $task)){
            abort(403, 'Unauthorized action.');
        }
        
        $pageInfo['page_name'] = "إدارة المهمات";
        $pageInfo['title'] = $task->name;

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('tasks_list')],
            ['title' => $pageInfo['title'], 'url' => route('tasks_view', ['id' => $task->id])],
        ];

        return view('tasks.view' , compact( 'pageInfo' , 'task' , 'model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('manage', Task::class)){
            abort(403, 'Unauthorized action.');
        }

        $model = TaskService::getOne($id);
        $pageInfo['form_method'] = 'PUT';
        $pageInfo['page_name'] = "إدارة المهمات";
        $pageInfo['title'] = $model->name;
        $pageInfo['form_url'] = url('tasks/'.$id);

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('tasks_list')],
            ['title' => $model->name, 'url' => route('tasks_view', ['id' => $model->id])],
            ['title' => 'تعديل', 'url' => route('tasks_edit', ['id' => $model->id])],
        ];

        $users = UserService::getList();

        $users = $users->mapWithKeys(function ($item , $key){
            return ([ strval($item->id)." " => $item->name]);
        })->all();


        return view('tasks.form' , compact( 'pageInfo' , 'users' , 'model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaskRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TaskRequest $request, $id)
    {
        if(!Auth::user()->can('manage', Task::class)){
            abort(403, 'Unauthorized action.');
        }

        $model = TaskService::getOne($id);
        $dataIn = $request->all();
        try {
            $model = TaskService::update($dataIn, $model);
        }catch (\Exception $e) {
            return redirect('tasks/'.$id.'/edit')->withInput()->withErrors(trans("all.edit-error"));
        }
        return redirect('tasks/'.$id.'/view')->with('success' , trans('all.success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if(!Auth::user()->can('manage', Task::class)){
            abort(403, 'Unauthorized action.');
        }

        $model = TaskService::delete($id);
        return redirect('tasks')->with('success' , trans('all.success'));
    }


    public function noteStore(TaskNoteRequest $request , $task_id )
    {
        $task = TaskService::getOne($task_id);

        if(!Auth::user()->can('view', $task)){
            abort(403, 'Unauthorized action.');
        }

        $model = New TaskNote();
        $dataIn = $request->all();
        $dataIn['task_id'] = $task->id;
        $dataIn['user_id'] = Auth::id();
        try {
            $model = TaskNoteService::create($dataIn, $model);
        }catch (\Exception $e) {
            return redirect('tasks/'.$task->id.'/view')->withInput()->withErrors(trans("all.create-error"));
        }
        return redirect('tasks/'.$task->id.'/view')->with('success' , trans('all.success'));
    }
}
