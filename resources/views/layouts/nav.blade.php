<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{!! url('/') !!}">ادارة علاقات العملاء</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-left">
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">تسجيل دخول</a></li>
                @else
                    <li><a href="{{ url('/users/'.Auth::id().'/view') }}"><i class="fa fa-user fa-fw"></i>الملف الشخصي</a>
                    </li>
                    @can('manage', \App\Models\User::class)
                    <li><a href="{{ url('/users') }}"><i class="fa fa-gear fa-fw"></i>إدارة المستخدمين</a>
                    </li>
                    @endcan
                    <li class="divider"></li>
                    <li><a href="{!! url("/logout") !!}"><i class="fa fa-sign-out fa-fw"></i> تسجيل الخروج</a>
                    </li>
                @endif
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->
    @if (Auth::check())
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="{!! url('/') !!}">الصفحة الرئيسية</a>
                </li>
                <li>
                    <a href="{!! url('tasks/') !!}"><i class="fa fa-tasks fa-fw"></i>إدارة المهمات</a>
                </li>
<li>
                    <a href="{!! url('orders/') !!}"><i class="fa fa-calendar-check-o fa-fw"></i>إدارة الطلبات</a>
                </li>
<li>
                    <a href="{!! url('invoices/') !!}"><i class="fa fa-money fa-fw"></i>إدارة الفواتير</a>
                </li>
<li>
                    <a href="{!! url('clients/') !!}"><i class="fa fa-address-card fa-fw" aria-hidden="true"></i>إدارة العملاء</a>
                </li>
                @can('manage', \App\Models\Department::class)
                <li>
                    <a href="{!! url('departments/') !!}"><i class="fa fa-puzzle-piece fa-fw"></i>إدارة الأقسام</a>
                </li>
                @endcan
                @can('manage', \App\Models\User::class)
                <li>
                    <a href="{!! url('users') !!}"><i class="fa fa-user fa-fw"></i>إدارة المستخدمين</a>
                </li>
                @endcan
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
    @endif
</nav>