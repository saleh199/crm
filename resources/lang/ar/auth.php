<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'failed'   => 'بيانات الاعتماد هذه غير متطابقة مع البيانات المسجلة لدينا.',
    'throttle' => 'عدد كبير جدا من محاولات الدخول. يرجى المحاولة مرة أخرى بعد :seconds ثانية.',
    'login' => 'تسجيل الدخول',
    'logout' => 'تسجيل الخروج',
    'reset-password' => 'اعادة تعيين كلمة المرور',
    'email' => 'البريد الإلكتروني',
    'send-reset-link' => 'ارسال رابط استعادة كلمة المرور',
    'password' => 'كلمة المرور',
    'confirm-password' => 'تأكيد كلمة المرور',
    'remember-me' => 'تذكرني',
    'forgot' => 'نسيت كلمة المرور؟',
];