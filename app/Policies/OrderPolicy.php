<?php

namespace App\Policies;

use App\Models\Order;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function manage(User $user)
    {
        return $user->isSuperAdmin();
    }

    public function view(User $user, Order $order)
    {
        return $user->id == $order->user_id;
    }
}
