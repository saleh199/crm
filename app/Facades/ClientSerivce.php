<?php

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class ClientService extends Facade
{
    protected static function getFacadeAccessor() { return 'ClientService'; }
}