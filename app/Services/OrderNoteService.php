<?php
/**
 * Created by PhpStorm.
 * User: amro
 * Date: 12/3/16
 * Time: 12:03 PM
 */
namespace App\Services;

use App\Models\OrderNote;

/**
 * Class OrderNoteService
 * @package App\Services
 */
class OrderNoteService{


    function __construct()
    {
    }

    /**
     * @param $criteria
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    function getList($criteria = []){
        $res = $this->resolveCriteria($criteria)->get();
        return $res;
    }

    function getOne($id){
        $res = OrderNote::findOrFail($id);
        return $res;
    }

    protected function resolveCriteria($data = [])
    {
        $query = OrderNote::Query();

        if (array_key_exists('columns', $data)) {
            $query = $query->select($data['columns']);
        }

        if (array_key_exists('order_note_id', $data)) {
            $query = $query->where('id', $data['order_note_id']);
        }
        if (array_key_exists('order_id', $data)) {
            $query = $query->where('order_id', $data['order_id']);
        }
        if (array_key_exists('user_id', $data)) {
            $query = $query->where('user_id', $data['user_id']);
        }

        if (array_key_exists('note_text', $data)) {
            $query = $query->where('note_text', 'LIKE'  , "%" . $data['note_text'] ."%");
        }

        if (array_key_exists('created_at', $data)) {
            $query = $query->where('created_at', "LIKE", "%".$data['created_at']."%");
        }

        if (array_key_exists('limit', $data) && array_key_exists('offset', $data)) {
            $query = $query->take($data['limit']);
            $query = $query->skip($data['offset']);
        }

        return $query;
    }

    function create( $dataIn = [], OrderNote &$OrderNote){
        $this->mapDataModel($dataIn , $OrderNote);

        $OrderNote->save();
    }

    public function mapDataModel($data, OrderNote &$model)
    {
        $attribute = [
            'note_text',
            'user_id',
            'order_id',
        ];

        foreach ($attribute as $val) {
            if (array_key_exists($val, $data)) {
                $model->$val = $data[$val];
            }
        }
    }

    function update($dataIn = [] , OrderNote &$OrderNote ){

        $this->mapDataModel($dataIn , $OrderNote);
        $OrderNote->save();
    }

    function delete($id){

        $res = $this->getOne($id);

        $res->delete();
    }


}
