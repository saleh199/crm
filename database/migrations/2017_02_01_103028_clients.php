<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table){
            $table->increments('id');
            $table->string('full_name' , '100');
            $table->string('email' , '100');
            $table->string('phone' , '100');
            $table->string('mobile' , '100')->nullable();
            $table->text('address')->nullable();
            $table->text('note')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->string('city' , '100')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExist('clients');
    }
}
