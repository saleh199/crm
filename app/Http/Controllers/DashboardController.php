<?php

namespace App\Http\Controllers;

use App\Facades\OrderService;
use App\Facades\TaskService;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $pageInfo = [
            'page_name' => 'الصفحة الرئيسية',
        ];

        // Task Counts
        $taskCounts = [
            0 => ['label' => 'قيد الإنتظار', 'count' => 0],
            1 => ['label' => 'قيد التنفيذ', 'count' => 0],
            2 => ['label' => 'نفذت', 'count' => 0],
        ];

        $taskCountsData = TaskService::getList([
            'columns' => DB::raw('COUNT(id) as count, status'),
            'group_by' => 'status',
        ])->toArray();

        foreach ($taskCountsData as $count) {
            $taskCounts[$count['status']]['count'] = $count['count'];
        }

        $taskCountsAll = collect($taskCounts)->sum('count');

        // Orders counts
        $orderCounts = [];

        for ($i = 1; $i <= 12; ++$i) {
            $orderCounts[$i] = [0 => 0, 1 => 0, 2 => 0];
        }

        $ordersCountsData = OrderService::getList([
            'columns' => DB::raw('COUNT(id) as count, status, YEAR(created_at) as year, MONTH(created_at) as month'),
            'group_by' => DB::raw('status, YEAR(created_at), MONTH(created_at)'),
            'year' => date('Y'),
        ])->toArray();

        foreach ($ordersCountsData as $count) {
            $orderCounts[$count['month']][$count['status']] = +$count['count'];
        }

        return view('dashboard.index', compact('pageInfo', 'taskCounts', 'taskCountsAll', 'orderCounts'));
    }
}
