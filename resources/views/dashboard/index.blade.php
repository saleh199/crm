@extends('layouts.master')

@section('title' , $pageInfo['page_name'])

@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x fa-flip-horizontal"></i>
                        </div>
                        <div class="col-xs-9 text-left">
                            <div class="huge">{!! $taskCountsAll !!}</div>
                            <div>مهمات جديدة</div>
                        </div>
                    </div>
                </div>
                <a href="{!! route('tasks_list') !!}">
                    <div class="panel-footer">
                        <span class="pull-right">عرض التفاصيل</span>
                        <span class="pull-left"><i class="fa fa-arrow-circle-left"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-8 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x fa-flip-horizontal"></i>
                        </div>
                        <div class="col-xs-9 text-left">
                            <div class="huge">3</div>
                            <div>طلبات جديدة</div>
                        </div>
                    </div>
                </div>
                <a href="{!! route('orders_list') !!}">
                    <div class="panel-footer">
                        <span class="pull-right">عرض التفاصيل</span>
                        <span class="pull-left"><i class="fa fa-arrow-circle-left"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <!-- /.panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw fa-flip-horizontal"></i> المهمات
                </div>
                <div class="panel-body">
                    <div id="morris-donut-chart"></div>
                    <a href="{!! route('tasks_list') !!}" class="btn btn-default btn-block">عرض التفاصيل</a>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <div class="col-lg-8">
            <!-- /.panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw fa-flip-horizontal"></i> الطلبات
                </div>
                <div class="panel-body">
                    <div id="morris-line-chart"></div>
                    <a href="{!! route('orders_list') !!}" class="btn btn-default btn-block">عرض التفاصيل</a>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <script type="application/javascript">
        $(function () {
            Morris.Donut({
                element: 'morris-donut-chart',
                data: [
                    @foreach($taskCounts as $taskCount)
                    { label: '{!! $taskCount['label'] !!}', value: '{!! $taskCount['count'] !!}' },
                    @endforeach
                ],
                colors: ['#d9534f', '#5bc0de', '#3f903f']
            });

            Morris.Line({
                element: 'morris-line-chart',
                data: [
                        @foreach($orderCounts as $index => $orderCount)
                    { month: new Date({!! date('Y') !!}, {!! $index - 1 !!}, 1).getTime(), pending: {!! $orderCount[0] !!}, inprogress: {!! $orderCount[1] !!}, done: {!! $orderCount[2] !!}},
                        @endforeach
                ],
                xkey: 'month',
                ykeys: ['pending', 'inprogress', 'done'],
                labels: ['طلبات قيد الانتظار', 'طلبات قيد التنفيذ', 'الطلبات المنفذة'],
                xLabels: ['month'],
                dateFormat: function(x){
                    return 'الشهر ' + new Date(x).getMonth();
                }
            });
        });
    </script>
@endsection