<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskNote extends Model
{
    function task(){
        return $this->belongsTo('App\Models\Task');
    }

    function user(){
        return $this->belongsTo('App\Models\User');
    }
}
