<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Invoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table){
            $table->increments('id');
            $table->text('description')->nullable();
            $table->dateTime('issue_date');
            $table->decimal('total',  10, 2);
            $table->string('currency', 3);
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('order_id');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices' , function (Blueprint $table){
            $table->dropForeign(['user_id']);
            $table->dropForeign(['order_id']);
        });
        Schema::dropIfExist('invoices');
    }
}
