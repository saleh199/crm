@extends('layouts.master')

@section('title' , $pageInfo['page_name'].' - '. $pageInfo['title'])


@section('content')

<div class="row">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($model, [ 'url' => $pageInfo['form_url'] , 'method' => 'post',  'class' => 'col-lg-12', 'files' => true]) !!}
        {{ method_field($pageInfo['form_method']) }}
        {!! Form::token() !!}

        <div class="form-group">
            {!! Form::label('الملاحظة') !!}
            {!! Form::textarea('note_text', null, ['placeholder' =>'الملاحظة', 'class' => 'form-control', 'id' => 'description']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit( "حفظ", ['class' => 'btn btn-primary']) !!}
            <a href="{{ url('/orders/'.$order->id.'/notes') }}" class="btn btn-warning">الغاء الأمر</a>
        </div>
    {!! Form::close() !!}

</div>
    <script type="text/javascript">
       $(document).ready(function (){
           $('#issue_date').datepicker({
               format: "yyyy-mm-dd",
               language: "ar",
               rtl: true,
               daysOfWeekDisabled: "5",
               daysOfWeekHighlighted: "5",
               autoclose: true })
       });
    </script>
@endsection
