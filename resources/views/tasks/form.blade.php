@extends('layouts.master')

@section('title' , $pageInfo['page_name'].' - '. $pageInfo['title'])


@section('content')

<div class="row">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($model, [ 'url' => $pageInfo['form_url'] , 'method' => 'post',  'class' => 'col-lg-12', 'files' => true]) !!}
        {{ method_field($pageInfo['form_method']) }}
        {!! Form::token() !!}

        <div class="form-group">
            {!! Form::label('المستخدم') !!}

                @if($model)
                    {!! Form::select('user_id', $users, $model->user_id, ['class' => 'selectpicker' , 'data-live-search'=>'true']) !!}
                @else
                    {!! Form::select('user_id', $users, old('user_id' , null), ['class' => 'selectpicker' , 'data-live-search'=>'true']) !!}
                @endif
        </div>

        <div class="form-group">
            {!! Form::label('عنوان المهمة') !!}
            {!! Form::text('name', null, ['placeholder' =>'عنوان المهمة', 'class' => 'form-control', 'id' => 'name']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('تاريخ المهمة') !!}
            {!! Form::text('issue_date', null, ['placeholder' =>'تاريخ المهمة', 'class' => 'form-control date', 'id' => 'issue_date']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('حالة المهمة') !!}
            {!! Form::select('status' , ['0' => 'قيد الإنتظار', '1' => 'قيد التنفيذ' , '2'=> 'نفذت']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('التفاصيل') !!}
            {!! Form::textarea('description', null, ['placeholder' =>'تفاصيل المهمة', 'class' => 'form-control', 'id' => 'description']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit( "حفظ", ['class' => 'btn btn-primary']) !!}
            <a href="{{ url('/tasks') }}" class="btn btn-warning">الغاء الأمر</a>
        </div>
    {!! Form::close() !!}

</div>
    <script type="text/javascript">
       $(document).ready(function (){
           $('#issue_date').datepicker({
               format: "yyyy-mm-dd",
               language: "ar",
               rtl: true,
               daysOfWeekDisabled: "5",
               daysOfWeekHighlighted: "5",
               autoclose: true })
       });
    </script>
@endsection
