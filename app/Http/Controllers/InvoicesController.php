<?php

namespace App\Http\Controllers;

use App\Facades\ClientService;
use App\Facades\InvoiceService;
use App\Facades\OrderService;
use App\Facades\UserService;
use App\Http\Requests\InvoiceRequest;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

/**
 * Class InvoicesController
 * @package App\Http\Controllers\Admin
 */
class InvoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */


    public function __construct()
    {
    }

    public function index()
    {
        $results = InvoiceService::getList();
        $pageInfo['page_name'] = "إدارة الفواتير";
        $pageInfo['title'] = "الفواتير";

        $pageInfo['breadcrumbs'] = [
            ['title' => $pageInfo['page_name'], 'url' => route('invoices_list')]
        ];

        return view('invoices.list' , compact( 'results', 'pageInfo'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = InvoiceService::delete($id);
        return redirect('invoices')->with('success' , trans('all.success'));
    }
}
