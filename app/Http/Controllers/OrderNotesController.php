<?php

namespace App\Http\Controllers;

use App\Facades\OrderNoteService;
use App\Facades\OrderService;
use App\Http\Requests\OrderNoteRequest;
use App\Models\OrderNote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class OrdersController
 * @package App\Http\Controllers\Admin
 */
class OrderNotesController extends Controller
{
    /**
     * @param $order_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function index($order_id)
    {
        $order = OrderService::getOne($order_id);

        $results =  OrderNoteService::getList(['order_id']);

        $pageInfo['page_name'] = "إدارة الملاحظات للطلب " . $order->title;
        $pageInfo['title'] = "الملاحظات";

        $pageInfo['breadcrumbs'] = [
            ['title' => "إدارة الطلبات", 'url' => route('orders_list')],
            ['title' => $order->title, 'url' => route('orders_view', ['id' => $order->id])],
            ['title' => $pageInfo['title'], 'url' => route('orders_notes_list', ['order_id' => $order->id])],
        ];

        return view( 'orders.notes.list' , compact(  'order' , 'results', 'pageInfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $order_id
     * @param null $model
     * @return \Illuminate\Http\Response
     */
    public function create( $order_id ,  $model = null)
    {
        $order = OrderService::getOne($order_id);
        $pageInfo['page_name'] = "إدارة الملاحظات للطلب " . $order->title;
        $pageInfo['title'] = "اضافة ملاحظة";
        $pageInfo['form_method'] = 'POST';
        $pageInfo['form_url'] = url('orders/'.$order->id.'/notes');

        $pageInfo['breadcrumbs'] = [
            ['title' => "إدارة الطلبات", 'url' => route('orders_list')],
            ['title' => $order->title, 'url' => route('orders_view', ['order_id' => $order->id])],
            ['title' => 'الملاحظات', 'url' => route('orders_notes_list', ['order_id' => $order->id])],
            ['title' => $pageInfo['title'], 'url' => route('orders_notes_create', ['order_id' => $order->id])],
        ];

        return view('orders.notes.form' , compact('pageInfo', 'order' , 'model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrderNoteRequest|Request $request
     * @param $order_id
     * @return \Illuminate\Http\Response
     */
    public function store(OrderNoteRequest $request , $order_id )
    {
        $order = OrderService::getOne($order_id);

        $model = New OrderNote();
        $dataIn = $request->all();
        $dataIn['order_id'] = $order->id;
        $dataIn['user_id'] = Auth::id();
        try {
            $model = OrderNoteService::create($dataIn, $model);
        }catch (\Exception $e) {
            return redirect('orders/'.$order->id.'/notes/create')->withInput()->withErrors(trans("all.create-error"));
        }
        return redirect('orders/'.$order->id.'/notes')->with('success' , trans('all.success'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $order_id
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($order_id , $id)
    {
        $order = OrderService::getOne($order_id);
        $model = OrderNoteService::getOne($id);
        $pageInfo['form_method'] = 'PUT';
        $pageInfo['page_name'] = "إدارة الملاحظات للطلب " . $order->title;
        $pageInfo['title'] = "تعديل ملاحظة";

        $pageInfo['form_url'] = url('orders/'.$order->id.'/notes/'.$id);

        $pageInfo['breadcrumbs'] = [
            ['title' => "إدارة الطلبات", 'url' => route('orders_list')],
            ['title' => $order->title, 'url' => route('orders_view', ['order_id' => $order->id])],
            ['title' => 'الملاحظات', 'url' => route('orders_notes_list', ['order_id' => $order->id])],
            ['title' => $pageInfo['title'], 'url' => route('orders_notes_edit', ['order_id' => $order->id, 'id' => $model->id])],
        ];


        return view('orders.notes.form' , compact( 'pageInfo' , 'order' , 'model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrderNoteRequest|Request $request
     * @param $order_id
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderNoteRequest $request,$order_id,  $id)
    {
        $order = OrderService::getOne($order_id);
        $model = OrderNoteService::getOne($id);
        $dataIn = $request->all();
        try {
            $model = OrderNoteService::update($dataIn, $model);
        }catch (\Exception $e) {
            return redirect('orders/'.$order->id.'/notes/'.$id.'/edit')->withInput()->withErrors(trans("all.edit-error"));
        }
        return redirect('orders/'.$order->id.'/notes')->with('success' , trans('all.success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $order_id
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $order_id , $id)
    {
        $order = OrderService::getOne($order_id);
        $model = OrderNoteService::delete($id);
        return redirect('orders/'.$order->id.'/notes')->with('success' , trans('all.success'));
    }
}
