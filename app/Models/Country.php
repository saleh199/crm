<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    function clients(){
        return $this->hasMany('App\Models\Client');
    }
}
