<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TaskNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_notes', function (Blueprint $table){
            $table->increments('id');
            $table->text('note_text');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('task_id');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_notes' , function (Blueprint $table){
            $table->dropForeign(['user_id'  , 'task_id']);
        });
        Schema::dropIfExist('task_notes');
    }
}
