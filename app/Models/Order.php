<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    function client(){
        return $this->belongsTo('App\Models\Client');
    }
    function user(){
        return $this->belongsTo('App\Models\User');
    }
    function notes(){
        return $this->hasMany('App\Models\OrderNote');
    }
    function invoice(){
        return $this->hasOne('App\Models\Invoice');
    }

}
