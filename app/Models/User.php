<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function departments()
    {
        return $this->belongsToMany('App\Models\Department', 'user_departments', 'user_id', 'department_id');
    }

    public function isSuperAdmin()
    {
        $departments = $this->departments()->get();

        foreach ($departments as $department){
            if($department->is_admin == 1){
                return true;
            }
        }

        return false;
    }
}
